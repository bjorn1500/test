﻿using System;
using System.Data.SqlClient;

namespace DBConnectie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef het alcohol gehalte:");
            var success = decimal.TryParse(Console.ReadLine(), out var gehalte);
            if (success)
            {
                var query = "SELECT Naam FROM bieren WHERE Alchohol = @Alcohol";
                using (var conn = new SqlConnection("Data Source=localhost; Initial Catalog=bieren_orgineel; User ID=sa; Password=SQL12345"))
                using (var command = new SqlCommand(query, conn))
                {
                    command.Parameters.AddWithValue("@Alcohol", gehalte);

                    conn.Open();

                    var reader = command.ExecuteReader();
                    while(reader.Read())
                    {
                        Console.WriteLine(reader["Naam"] ?? "");
                    }

                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("parsing failed! bye bye!");
                Console.ReadLine();
            }
        }
    }
}
